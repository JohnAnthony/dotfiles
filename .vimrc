:set nocompatible

let b:plugNew = 0
if !filereadable($HOME . '/.vim/autoload/plug.vim')
	let b:plugNew = 1
end

if b:plugNew == 1
	:silent !curl -fL -o ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
end

call plug#begin($HOME . '/.vim/plugged')
Plug 'Valloric/YouCompleteMe'
Plug 'airblade/vim-gitgutter'
Plug 'altercation/vim-colors-solarized'
Plug 'enricobacis/vim-airline-clock'
Plug 'fatih/vim-go'
Plug 'flazz/vim-colorschemes'
Plug 'jacoborus/tender.vim'
Plug 'jparise/vim-graphql'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'ntpeters/vim-better-whitespace'
Plug 'prettier/vim-prettier'
Plug 'qpkorr/vim-bufkill'
Plug 'scrooloose/nerdtree'
Plug 'sheerun/vim-polyglot'
Plug 't9md/vim-choosewin'
Plug 'tpope/vim-fireplace'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sleuth'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'weilbith/nerdtree_choosewin-plugin'
Plug 'chaimleib/vim-renpy'
call plug#end()

:set encoding=utf-8
:set tabstop=4
:set shiftwidth=2
:set wildmode=longest,list
:set wildmenu
:set backspace=2
:set number
:set colorcolumn=80
:set nobackup
:set nowritebackup
:set noswapfile
:set equalalways
:set guioptions=
:set nobackup
:set list
:set listchars=tab:>-
:set nowrap
:set foldmethod=indent
:set nofoldenable
:set foldclose=

" appearance
:set term=screen-256color
:set background=dark
:set guifont=Terminus\ 9
:set noantialias
:syntax enable
:colorscheme tender
:highlight LineNr guifg=#777777
:highlight SignColumn guibg=#282828
:highlight Visual cterm=none ctermfg=black ctermbg=yellow guifg=black guibg=yellow

" search
:set hlsearch
:nnoremap <silent> <C-l> :<C-u>nohlsearch<cr><C-l>

:set guicursor+=a-v:blinkon0
:set guicursor+=i:ver20-blinkon0-iCursor
:highlight Cursor guifg=white guibg=orange
:highlight iCursor guifg=white guibg=red

if b:plugNew == 1
	PlugUpdate --sync
	execute ':q'
	echo 'Package manager was missing. Fixed!'
end

:nmap <C-n> :NERDTreeToggle<cr>
:nmap \a :Ag<cr>
:nmap \j :Buffers<cr>
:nmap \f :Files<cr>
:nmap \w :ChooseWin<cr>
:nmap \p :Prettier<cr>
:nmap \gt :GoTest<cr>
:let g:NERDTreeNodeDelimiter = "\u00a0"
:let g:NERDTreeChDirMode = 2
:let g:NERDMinimalUI = 1
:let macvim_skip_colorscheme = 1
:let g:choosewin_overlay_enable = 1
:let g:BufKillActionWhenBufferDisplayedInAnotherWindow = 'remove'
:let g:airline_theme = 'jellybeans'
:let $FZF_DEFAULT_COMMAND = 'ag -S'
:autocmd BufWritePost * GitGutter
" :let g:ycm_filetype_blacklist = { 'javascript': 1 }

" font zooming
:nmap <m--> <Plug>(fontzoom-smaller)
:nmap <m-=> <Plug>(fontzoom-larger)

set statusline+=%#warningmsg#
set statusline+=%*

:let g:gitgutter_sign_added = '+'
:let g:gitgutter_sign_modified = '~'
:let g:gitgutter_sign_removed = '-'
:let g:gitgutter_sign_removed_first_line = 'x'
:let g:gitgutter_sign_modified_removed = 'x'
