source ~/.zshrc-grml
source ~/.profile

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt extendedglob appendhistory
bindkey -v
