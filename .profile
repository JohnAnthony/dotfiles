if [ ! -f "/tmp/ssh-agent" ]; then
	ssh-agent | grep -v '^echo' > /tmp/ssh-agent
	chmod +x /tmp/ssh-agent
fi
. /tmp/ssh-agent

export _JAVA_AWT_WM_NONREPARENTING=1

export GOBIN="$HOME/go/bin"
export PATH="$HOME/bin:$GOBIN:$HOME/.node_modules/bin:$PATH"

export EDITOR="vim"
export ALTERNATE_EDITOR="vi"

LS_COLORS='*=1;37:di=1;31:ex=0;34:*.exe=0;34:*.com=0;34:*.wmv=1;35:*.avi=1;35:*.flv=1;35:*.webm=1;35:*.mkv=1;35:*.mpg=1;35:*.mpeg=1;35:*.ogv=1;35:*.jpg=1;33:*.jpeg=1;33:*.png=1;33:*.gif=1;33:*.bmp=1;33:*.rar=0;32:*.zip=0;32:*.tar.gz=0;32:*.tar.bz=0;32:*.tar.gz*=0;32:*.tar.bz*=0;32:*.pdf=0;36:*.chm=0;36:*.pl=0;34:*.py=0;34:*.cpp=0;34:*.c=0;34:*.hpp=0;34:*.h=0;34:*.hs=0;34:*.css=0;34:*.php=0;34:*.html=0;34:*.htm=0;34:*.css=0;34:*.js=0;34:*.hta=0;34:*.cbr=0;36:*.cbz=0;36:*.mp3=1;35:*.ogg=1;35:*.wav=1;35:*.mp4=1;35:*.7z=0;32'

alias s=sudo
alias z=zathura
alias stamp="date +%s-%N"
alias gdb="gdb -q"
alias sprunge='curl -F "sprunge=<-" http://sprunge.us'
alias pb='qiv -muw 1920'
alias pbshuf='qiv -Smuw 1920'
alias mfs="mplayer -fs -shuffle"
alias trr=transmission-remote
alias trd=transmission-daemon
alias beep='aplay ~/.beep.wav'
alias timestamp='date "+%s"'
alias x='exa -s modified -al'
alias e=exa
alias t='transmission-remote -l'
alias g=git
alias d=docker
alias doc=docker-compose
alias m=mplayer
alias shi='sh *.sh'
alias k=kubectl
alias ss=simplesrv
alias bt=bluetoothctl

# Crypto
alias sopen='if [[ ! -e /dev/mapper/store ]]; then sudo cryptsetup luksOpen /dev/sdc1 store; sudo mount /dev/mapper/store /mnt/store; fi; cd /mnt/store'
alias sclose='sudo umount /mnt/store; s cryptsetup luksClose store'

alias sp="sudo pacman"
alias p=pacman

export TEXMFLOCAL="/home/ja/.local/share:$TEXMFLOCAL"

function nvminit() {
	unset npm_config_prefix
	export NVM_DIR="$HOME/.nvm"
	[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
}

#### RAINBIRD

alias sdev='skaffold dev --trigger=manual --status-check=false --cleanup=false'
export SKAFFOLD_NAMESPACE=developer-ja
export SKAFFOLD_PORT_FORWARD=true
export SKAFFOLD_DEFAULT_REPO=gcr.io/gitlab-ci-cd-254609
export VERSION=development
